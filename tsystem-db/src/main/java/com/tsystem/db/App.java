/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.db;

import com.tsystem.dm.Employee1;
import com.tsystem.dm.entity.Address;
import com.tsystem.dm.entity.Department;
import com.tsystem.dm.entity.Employee;
import com.tsystem.dm.entity.Project;
import com.tsystem.dm.entity.Role;
import com.tsystem.dm.entity.Task;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;
import javax.persistence.Persistence;

/**
 *
 * @author avincze
 */
public class App {
    
    public static void main(String[] args) {
        Employee1 employee1 = new Employee1();
        employee1.setFirstName("first name 1");
        employee1.setLastName("last name 1");
        employee1.setLoginName("login name 1");
        employee1.setPassword("password 1");
        employee1.setSalary(11L);
        
        Persistence.createEntityManagerFactory("tlogs-db-pu");
        initTSystems("tsystem-db-pu");
        initTSystems("tsystem2-db-pu");
        
        //EntityManager em = factory.createEntityManager();
        
        
        
        
        
    }
    
    
    private static void initTSystems(String unitName){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(unitName);
        EntityManager em = factory.createEntityManager();
        EntityManager em2 = factory.createEntityManager();
        
        em.getTransaction().begin();
        Department department = new Department("department1");
        em.persist(department);
        Department department2 = new Department("department2");
        em.persist(department2);
        Role role1 = new Role("admin");
        em.persist(role1);
        Role role2 = new Role("developer");
        em.persist(role2);

        Employee employee = new Employee();
        employee.setFirstName("firstname1");
        employee.setLastName("lastname1");
        employee.setUserName("username1");
        employee.setDepartment(department);
        employee.setAddress(new Address("zip1", "city1", "street1"));
        employee.setRoleList(new HashSet<>());
        employee.getRoleList().add(role1);
        employee.getRoleList().add(role2);
        employee.setPassword("password1");
        employee.setSalary(100L);
        //em.persist(employee);

        Employee employee2 = new Employee();
        employee2.setFirstName("firstname2");
        employee2.setLastName("lastname2");
        employee2.setUserName("username2");
        employee2.setDepartment(department2);
        employee2.setAddress(new Address("zip2", "city2", "street2"));
        employee2.setRoleList(new HashSet<>());
        employee2.getRoleList().add(role1);
        employee2.getRoleList().add(role2);
        employee2.setPassword("password2");
        employee2.setSalary(200L);
        //em.persist(employee2);
        
        employee.setBoss(employee2);
        employee2.setBoss(employee);

        em.persist(employee);
        em.persist(employee2);
        
        Project project = new Project();
        project.setName("project1");
        project.setStartDate(new Date());
        project.setEndDate(new Date(project.getStartDate().getTime() + 7 * 24 * 60 * 60 * 1000));
        project.setOwner(employee);
        project.setTaskList(new ArrayList<>());

        Task task = new Task("task1");
        task.setParticipant(employee);
        project.getTaskList().add(task);
        task = new Task("task11");
        task.setParticipant(employee2);
        project.getTaskList().add(task);

        em.persist(project);

        Project project2 = new Project();
        project2.setName("project2");
        project2.setStartDate(new Date());
        project2.setEndDate(new Date(project2.getStartDate().getTime() + 14 * 24 * 60 * 60 * 1000));
        project2.setOwner(employee2);
        project2.setTaskList(new ArrayList<>());

        Task task2 = new Task("task2");
        task2.setParticipant(employee);
        project2.getTaskList().add(task2);
        task2 = new Task("task22");
        task2.setParticipant(employee);
        project2.getTaskList().add(task2);
        em.persist(project2);
        
        //em.lock(project, LockModeType.PESSIMISTIC_WRITE);
        project.setName("project11");

        em.getTransaction().commit();
        
        
        
        
        em.getTransaction().begin();
        em2.getTransaction().begin();
        
        Department dep1 = em.find(Department.class, 1L);
        //Department dep11 = em2.find(Department.class, 1L, LockModeType.PESSIMISTIC_WRITE);
        
        //em.lock(dep1, LockModeType.PESSIMISTIC_WRITE);
        //dep1.setName("dep11");
        //em.merge(dep1);
        em.getTransaction().commit();
        em2.getTransaction().commit();
        
        factory.close();
        
    }
}
