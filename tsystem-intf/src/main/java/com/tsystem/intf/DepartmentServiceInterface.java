/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.intf;

import com.tsystem.dm.entity.Department;

/**
 *
 * @author avincze
 */
public interface DepartmentServiceInterface {

    void save(Department department) throws MyException;
        
}
