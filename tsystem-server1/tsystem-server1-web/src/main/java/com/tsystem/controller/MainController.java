/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.controller;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author avincze
 */
@Named
@SessionScoped
public class MainController implements Serializable{
    private String userName;

    //@Resource
    //private SessionContext context;
    
    //@RolesAllowed(value = {"USER"})

    public MainController() {
        this.userName = "un";
        System.out.println("hello");
    }
    
    @PostConstruct
    public void init(){
        if (FacesContext.getCurrentInstance()
                .getExternalContext().isUserInRole("AAA")) {
           this.userName = 
                FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
 
        }else {
            this.userName = "bbb";
        }
        
    }
    
    
    
   
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
