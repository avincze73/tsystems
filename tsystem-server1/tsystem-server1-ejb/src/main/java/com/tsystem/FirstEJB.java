/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem;

import com.tsystem.intf.FirstRemoteInterface;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;

/**
 *
 * @author martin
 */
@Stateless
@Remote(FirstRemoteInterface.class)
public class FirstEJB implements FirstRemoteInterface {

    @Override
    public String echo() {
        return "echo from EJB";
    }

}
