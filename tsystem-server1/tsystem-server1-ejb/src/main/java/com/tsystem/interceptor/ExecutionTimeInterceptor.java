/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.interceptor;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author avincze
 */
public class ExecutionTimeInterceptor {
    
    @AroundInvoke
    public Object interceptBusinessMethod(InvocationContext context) throws Exception{
        LocalTime start = LocalTime.now();
        Object ret = context.proceed();
        //System.out.println(context.getMethod().getParameters().length);
        int time = (LocalTime.now().getNano() - start.getNano()) / 1000;
        String stime = new DecimalFormat("#,###").format(time);
        Logger.getLogger(getClass().getName()).log(Level.INFO, 
        context.getMethod().getName() + ": " + stime + " ms");      
        return ret;
    }
    
}
