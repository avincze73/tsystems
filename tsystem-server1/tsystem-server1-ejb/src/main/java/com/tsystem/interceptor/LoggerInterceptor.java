/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.interceptor;

import com.tsystem.intf.LoggerServiceInterface;
import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author avincze
 */
public class LoggerInterceptor {
    
    
    @EJB(lookup = "com.tsystem.intf.LoggerServiceInterface")
    private LoggerServiceInterface loggerServiceInterface;
    
    @AroundInvoke
    public Object interceptBusinessMethod(InvocationContext context) throws Exception{
        loggerServiceInterface.log(context.getMethod().getName(), "TRACE");
        return context.proceed();
    }
    
}
