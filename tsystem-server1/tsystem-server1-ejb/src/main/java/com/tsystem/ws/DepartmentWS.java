/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.ws;

import com.tsystem.dto.SaveDepartmentRequest;
import com.tsystem.dto.SaveDepartmentResponse;
import com.tsystem.facade.DepartmentFacade;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;

/**
 *
 * @author avincze
 */
@WebService(serviceName = "DepartmentWS")
@Stateless()
public class DepartmentWS {

    @EJB
    private DepartmentFacade departmentFacade;

    /**
     * This is a sample web service operation
     */
//    @WebMethod(operationName = "hello")
//    public String hello(@WebParam(name = "name") String txt) {
//        return "Hello " + txt + " !";
//    }
    public SaveDepartmentResponse saveDepartment(SaveDepartmentRequest request) {
        return departmentFacade.saveDepartment(request);
    }
}
