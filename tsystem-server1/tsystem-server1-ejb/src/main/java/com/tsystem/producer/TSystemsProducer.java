package com.tsystem.producer;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class TSystemsProducer {

    @Produces
    @PersistenceContext(unitName="tsystem-server1-ejb-pu")
    @TSystemsDatabase
    EntityManager em1;

    @Produces
    @PersistenceContext(unitName="tsystem2-server1-ejb-pu")
    @TSystems2Database
    EntityManager em2;
}
