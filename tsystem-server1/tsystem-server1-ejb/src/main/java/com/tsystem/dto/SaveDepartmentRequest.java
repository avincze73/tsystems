/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.dto;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author avincze
 */
@XmlRootElement()
@XmlType(name = "SaveDepartmentInputMessage")
public class SaveDepartmentRequest {
    private String departmentName;

    public SaveDepartmentRequest() {
    }

    public SaveDepartmentRequest(String departmentName) {
        this.departmentName = departmentName;
    }

     
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    
    
}
