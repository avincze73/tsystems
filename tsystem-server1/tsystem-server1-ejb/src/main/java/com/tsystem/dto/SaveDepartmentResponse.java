/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.dto;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author avincze
 */
@XmlRootElement()
@XmlType(name = "SaveDepartmentOutputMessage")
public class SaveDepartmentResponse {

    private String status;

    public SaveDepartmentResponse() {
    }

    public SaveDepartmentResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
