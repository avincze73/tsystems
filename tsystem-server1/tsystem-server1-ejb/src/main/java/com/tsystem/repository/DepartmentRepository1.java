/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.repository;

import com.tsystem.dm.entity.Department;
import com.tsystem.dm.entity.Employee;
import com.tsystem.interceptor.ExecutionTimeInterceptor;
import com.tsystem.interceptor.LoggerInterceptor;
import com.tsystem.intf.DepartmentRepository;
import com.tsystem.intf.LoggerServiceInterface;
import com.tsystem.intf.MyException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.TransactionAttribute;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
//@Remote(DepartmentRepository.class)
//@Interceptors({ExecutionTimeInterceptor.class,LoggerInterceptor.class})
public class DepartmentRepository1{

    @PersistenceContext(unitName = "tsystem-server1-ejb-pu")
    private EntityManager em;
    
    @EJB(lookup = "com.tsystem.intf.LoggerServiceInterface")
    private LoggerServiceInterface loggerServiceInterface;
    
    
    @Resource
    private SessionContext context;
    
    //@TransactionAttribute()
    public void save(Department department) throws MyException{
        em.merge(department);
        
        loggerServiceInterface.log("save is called", "TRACE");
        //context.setRollbackOnly();
        //throw new MyException("my error");
    }
    
    public List<Department> findAll(){
        //context.setRollbackOnly();
        return em
                .createQuery("select d from Department d", Department.class)
                .getResultList();
    }
    
    
}
