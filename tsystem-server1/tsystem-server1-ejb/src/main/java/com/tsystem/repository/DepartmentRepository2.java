/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.repository;

import com.tsystem.dm.entity.Department;
import com.tsystem.interceptor.ExecutionTimeInterceptor;
import com.tsystem.interceptor.LoggerInterceptor;
import com.tsystem.intf.MyException;
import com.tsystem.producer.TSystems2Database;
import com.tsystem.producer.TSystemsDatabase;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

/**
 *
 * @author avincze
 */
//@Interceptors({ExecutionTimeInterceptor.class,LoggerInterceptor.class})
@Transactional(Transactional.TxType.REQUIRED)
public class DepartmentRepository2{

    @Inject
    @TSystems2Database
    private EntityManager em;

    @Transactional(Transactional.TxType.REQUIRED)
    public void save(Department department) throws MyException{
        em.merge(department);
    }
}
