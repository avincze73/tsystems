package com.tsystem.schedule;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
@LocalBean
public class ScheduledServices {

    @Inject
    @JMSConnectionFactory( "jms/tsystemsfactory")
    private JMSContext jmsContext;
    @Resource(lookup = "jms/tsystems")
    private Queue queue1;
    public void sendMessageToQueue() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Sending message to jms queue");
        String text = LocalDateTime.now().toString();
        jmsContext.createProducer().send(queue1, text);
    }
    @Schedule(second = "30", minute = "0/1", hour = "*", info = "ScheduledServices.echoToLog timer", persistent = false)
    public void echoToLog() {
        sendMessageToQueue();
    }

}
