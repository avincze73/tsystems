/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.service;

import com.tsystem.dm.entity.Department;
import com.tsystem.intf.DepartmentServiceInterface;
import com.tsystem.intf.MyException;
import com.tsystem.repository.DepartmentRepository1;
import com.tsystem.repository.DepartmentRepository2;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.inject.Inject;

/**
 *
 * @author avincze
 */
@Stateless
//@LocalBean
@Remote(com.tsystem.intf.DepartmentServiceInterface.class)
public class DepartmentService implements DepartmentServiceInterface{

    @EJB
    private DepartmentRepository1 departmentRepository1;
    
    @Inject
    private DepartmentRepository2 departmentRepository2;        
          
    public void save(Department department) throws MyException{
        departmentRepository1.save(department);
        departmentRepository2.save(department);
    }
}
