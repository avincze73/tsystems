/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.facade;

import com.tsystem.dm.entity.Department;
import com.tsystem.dto.SaveDepartmentRequest;
import com.tsystem.dto.SaveDepartmentResponse;
import com.tsystem.intf.DepartmentServiceInterface;
import com.tsystem.intf.MyException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
public class DepartmentFacade {

    @EJB
    private DepartmentServiceInterface departmentService;

    
    public SaveDepartmentResponse saveDepartment(SaveDepartmentRequest request){
        SaveDepartmentResponse response = new SaveDepartmentResponse("OK");
        Department department = new Department(request.getDepartmentName());
        try {
            departmentService.save(department);
        } catch (MyException ex) {
            response.setStatus("ERROR");
            Logger.getLogger(DepartmentFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
}
