/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.cli;

import com.tsystem.dm.entity.Department;
import com.tsystem.intf.DepartmentRepository;
import com.tsystem.intf.DepartmentServiceInterface;
import com.tsystem.intf.FirstRemoteInterface;
import com.tsystem.intf.MyException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author avincze
 */
public class App {
    public static void main(String[] args) {
        try {
            InitialContext jndi = new InitialContext();
//            FirstRemoteInterface service = 
//                    (FirstRemoteInterface) 
//                    //jndi.lookup("java:global/tsystem-server1-ear/tsystem-server1-ejb-1.0-SNAPSHOT/FirstEJB!com.tsystem.intf.FirstRemoteInterface");
//                    jndi.lookup("com.tsystem.intf.FirstRemoteInterface");
//            System.out.println(service.echo());
            
            
//            Department department = new Department("d2");
//            DepartmentRepository departmentRepository =
//            (DepartmentRepository) jndi.lookup("com.tsystem.intf.DepartmentRepository");
//            try {
//                departmentRepository.save(department);
//            } catch (MyException ex) {
//                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            
//            departmentRepository.findAll()
//                    .forEach(
//                            (d)->System.out.println(d.getName())
//                            );
//            
            
            
            Department department = new Department("d2");
            DepartmentServiceInterface departmentService =
            (DepartmentServiceInterface) jndi.lookup("com.tsystem.intf.DepartmentServiceInterface");
            try {
                departmentService.save(department);
            } catch (MyException ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
            
        } catch (NamingException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
