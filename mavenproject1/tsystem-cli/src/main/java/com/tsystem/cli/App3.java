/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.cli;

import wsdep.DepartmentWS_Service;
import wsdep.SaveDepartment;
import wsdep.SaveDepartmentInputMessage;

/**
 *
 * @author avincze
 */
public class App3 {
    public static void main(String[] args) {
        DepartmentWS_Service client = new DepartmentWS_Service();
        SaveDepartment saveDepartment = new SaveDepartment();
        SaveDepartmentInputMessage im = new SaveDepartmentInputMessage();
        im.setDepartmentName("d3");
        saveDepartment.setArg0(im);
        client.getDepartmentWSPort().saveDepartment(saveDepartment);
    }
}
