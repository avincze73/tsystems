package com.tsystem.dm.entity;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Entity implementation class for Entity: Department
 *
 */
@Entity
public class TSystemsLog extends TSystemsEntity {

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    private String message;
    private String severity;
    private static final long serialVersionUID = 1L;

    public TSystemsLog() {
        this(null);
    }

    public TSystemsLog(String message) {
        this.created = new Date();
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

}
