package com.tsystem.dm.entity;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Address
 *
 */
@Embeddable
public class Address {

	@Column(name="zip")
	private String zip;
	@Column(name="city")
	private String city;
	@Column(name="street")
	private String street;

	//@Column(name = "firstName")
	//private String firstName;

	private static final long serialVersionUID = 1L;

	public Address() {
		super();
	}

	public Address(String zip, String city, String street) {
		// TODO Auto-generated constructor stub
		this.zip = zip;
		this.city = city;
		this.street = street;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}


}
