package hu.cib.producer;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CibProducer {

    @Produces
    @PersistenceContext(unitName="cib2-server1-pu1")
    @Cib1Database
    EntityManager em1;

    @Produces
    @PersistenceContext(unitName="cib2-server1-pu2")
    @Cib2Database
    EntityManager em2;

}
