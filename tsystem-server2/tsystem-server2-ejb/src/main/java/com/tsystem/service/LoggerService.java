/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsystem.service;

import com.tsystem.dm.entity.TSystemsLog;
import com.tsystem.intf.LoggerServiceInterface;
import com.tsystem.intf.MyException;
import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author martin
 */
@Stateless
@LocalBean
@Remote(LoggerServiceInterface.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class LoggerService {

    @PersistenceContext(unitName = "tsystem-server1-ejb-pu")
    private EntityManager em;
    
    @Resource
    private SessionContext context;
    
    public void log(String message, String severity) throws MyException{
        TSystemsLog log = new TSystemsLog(message);
        log.setSeverity(severity);
        em.merge(log);
        //throw new Exception("my error");
        context.setRollbackOnly();
    }
}
